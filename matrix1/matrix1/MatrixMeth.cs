﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrix1
{
    class MatrixMeth
    {
        public static void numberPos(int[,] m)
        {
            int k = m.GetLength(0);
            int n = m.GetLength(1);
            int j = 0;
            int num = 0;
            int i;
            bool flag = false;
            while ((j < n) && (num == 0))
            {
                flag = false;
                i = 0;
                while ((i < k) && (!flag))
                {
                    if (m[i, j] < 0) { flag = true; }
                    i++;
                }
                if (!flag) num = j + 1;
                j++;
            }
            if (num > 0) Console.WriteLine("Перший стовпець, в якому немає вiд'ємних елементiв, має номер " + num);
            else Console.WriteLine("В матрицi немає стовпцiв з усiма невiд'ємними елементами! ");
        }
        static public void order(int[,] m)
        {

            int k = m.GetLength(0);
            int n = m.GetLength(1);
            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(m[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            int[] rep = new int[k];
            int max = 1;
            int count = 0;
            for (int i = 0; i < k; i++)
            {
                max = 1;
                for (int j = 0; j < n; j++)
                {
                    count = 0;
                    for (int l = 0; l < n; l++)
                    {
                        if (m[i, l] == m[i, j]) count++;
                    }
                    if (count > max) max = count;
                }
                rep[i] = max;
            }

            int maxRep = 1;
            for (int i = 0; i < k; i++)
            {
                if (rep[i] > maxRep) maxRep = rep[i];
            }
            for (int l = 1; l <= maxRep; l++)
            {
                for (int i = 0; i < k; i++)
                {
                    if (rep[i] == l)
                    {
                        for (int j = 0; j < n; j++) Console.Write(m[i, j] + " ");
                        Console.WriteLine();
                    }
                }
            }


        }

    }
}
