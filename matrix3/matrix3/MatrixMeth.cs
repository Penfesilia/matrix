﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrix3
{
    class MatrixMeth
    {
        static public void FillMatrix(int[,] m)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    m[i, j] = i - j + 1;
                }
            }
        }
        static public void FindK(int[,] m)
        {
            int i = 0;
            int k = 0;
            bool flag;
            while (k < 8)
            {
                flag = true;
                i = 0;
                while ((i < 8) && (flag))
                {
                    flag = (m[k, i] == m[i, k]);
                    i++;
                }
                if (flag) Console.WriteLine((k + 1) + "-ий рядок співпадає з " + (k + 1) + "-им стовпцем!");
                k++;
            }
        }
        public static void sum(int[,] m)
        {
            bool[] isNeg = new bool[8];
            int j = 0;
            int sum = 0;
            int i = 0;
            bool flag = false;
            while (i < 8)
            {
                flag = false;
                j = 0;
                while ((j < 8) && (!flag))
                {
                    if (m[i, j] < 0)
                    { flag = true; }
                    j++;
                }
                if (!flag) isNeg[i] = false;
                else isNeg[i] = true;
                i++;
            }
            for (int l = 0; l < 8; l++)
            {
                if (isNeg[l])
                {
                    sum = 0;
                    for (int p = 0; p < 8; p++)
                    {
                        sum += m[l, p];
                    }
                    Console.WriteLine("Сума елементiв в " + (l + 1) + " рядку становить " + sum);
                }
            }
        }
    }
}
