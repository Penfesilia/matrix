﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrix2
{
    class MatrixMeth
    {
        public static void sum(int[,] m)
        {
            int k = m.GetLength(0);
            int n = m.GetLength(1);
            bool[] isNeg = new bool[k];
            int j = 0;
            int sum = 0;
            int i = 0;
            bool flag = false;
            while (i < k)
            {
                flag = false;
                j = 0;
                while ((j < n) && (!flag))
                {
                    if (m[i, j] < 0)
                    { flag = true; }
                    j++;
                }
                if (!flag) isNeg[i] = false;
                else isNeg[i] = true;
                i++;
            }
            for (int l = 0; l < k; l++)
            {
                if (isNeg[l])
                {
                    sum = 0;
                    for (int p = 0; p < n; p++)
                    {
                        sum += m[l, p];
                    }
                    Console.WriteLine("Сума елементiв в " + (l + 1) + " рядку становить " + sum);
                }
            }
        }
        public static void sidl(int[,] m)
        {
            int k = m.GetLength(0);
            int n = m.GetLength(1);
            int[] min = new int[k];
            for (int i = 0; i < k; i++)
            {
                min[i] = m[i, 0];
                for (int j = 1; j < n; j++)
                {
                    if (m[i, j] < min[i]) min[i] = m[i, j];
                }
            }
            int[] max = new int[n];
            for (int j = 0; j < n; j++)
            {
                max[j] = m[0, j];
                for (int i = 1; i < k; i++)
                {
                    if (m[i, j] > max[j]) max[j] = m[i, j];
                }
            }
            Console.WriteLine("Сiдловi точки матрицi:");
            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (min[i] == max[j]) Console.WriteLine("matr[" + (i + 1) + " ," + (j + 1) + "]=" + m[i, j]);
                }

            }
        }

    }

}
